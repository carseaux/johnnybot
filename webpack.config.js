const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  mode: 'development',
  entry: './src/main.js',
	output: {
		path: __dirname + '/dist/',
		filename: 'bundle.js'
  },
  module: {
    rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel-loader',
      },
      {
        test: /\.scss$/,
				use: [
				  { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'sass-loader' }
				]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: __dirname + '/src/index.html'
    })
  ],
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    port: 3000,
    open: true
  }
}