import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import messagesReducer from './messages/reducer'

const reducers = combineReducers({ messages: messagesReducer })
export default createStore(reducers, applyMiddleware(thunk, logger))