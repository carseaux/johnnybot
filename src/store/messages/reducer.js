import * as constants from '../constants'
import Chatbot from '../../systems/chatbot'

const initialState = {
  messages: [Chatbot.questions[0]]
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case constants.ADD_MESSAGE: {
      const messages = [...state.messages, action.payload]
      return { ...state, messages }
    }
    case constants.REQUEST_NEXT_QUESTION: {
      const nextQuestion = Chatbot.requestNextQuestion(action.payload)
      if (nextQuestion) {
        return { ...state, messages: [...state.messages, nextQuestion] }
      }
      return { ...state }
    }
    default: {
      return state
    }
  }
}

export default reducer