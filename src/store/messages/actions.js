import * as constants from '../constants'

export const addMessage = (payload) => {
  return {
    type: constants.ADD_MESSAGE,
    payload: payload
  }
}

export const requestNextQuestion = (questionIndex, answerIndex) => {
  return {
    type: constants.REQUEST_NEXT_QUESTION,
    payload: { questionIndex, answerIndex }
  }
}