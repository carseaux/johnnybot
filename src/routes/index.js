import React from 'react'
import Chat from '../components/chat'

class IndexRoute extends React.Component {
  render () {
    return <div className='route'>
      <Chat />
    </div>
  }
}

export default IndexRoute