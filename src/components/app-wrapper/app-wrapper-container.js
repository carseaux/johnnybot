import { connect } from 'react-redux'
import AppWrapperComponent from './app-wrapper-component'

const mapState = (state, props) => {
  return {}
}

const mapDispatch = (dispatch, props) => {
  return {}
}

export default connect(mapState, mapDispatch)(AppWrapperComponent)