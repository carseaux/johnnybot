import React from 'react'
import './style/chat.scss'

class Chat extends React.Component {
  onKeyDown = (e) => {
    if (e.keyCode === 13) {
      if (!e.shiftKey) {
        e.preventDefault()
        e.stopPropagation()
        const message = e.target.value
        this.props.actions.addMessage({ type: 'message', message: message, timestamp: new Date() })
        e.target.value = ''
      }
    }
  }

  selectAnswer (e, questionIndex, answerIndex) {
    const parentNode = e.currentTarget.parentNode
    parentNode.classList.add('answered')
    parentNode.querySelectorAll('.answer').forEach(answer => (answer === e.currentTarget) ? answer.classList.add('selected') : answer.classList.add('hidden'))
    setTimeout(() => this.props.actions.requestNextQuestion(questionIndex, answerIndex), 400)
  }

  render () {
    const messages = this.props.messages.map((msg, i) => {
      if (msg.type === 'message') {
        return <div key={i} className='message-wrapper'>
          <div className='message'>{ msg.message }</div>
        </div>
      } else {
        const answers = msg.answers.map((answer, j) => {
          return <div key={j} className='answer' onClick={ (e) => this.selectAnswer(e, msg.index, j) }>{ answer }</div>
        })

        return <div key={i} className='question-wrapper'>
          <div className='question'>{ msg.question }</div>
          <div className='answers'>{ answers }</div>
        </div>
      }
    })
    
    return <div className='chat'>
      <div className='output'>{ messages }</div>
      <div className='input'>
        <textarea onKeyDown={ this.onKeyDown } />
      </div>
    </div>
  }
}

export default Chat
