import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import ChatComponent from './chat-component'

import * as messageActions from '../../store/messages/actions'

const mapState = ({ messages }, props) => {
  return {
    messages: messages.messages
  }
}

const mapDispatch = (dispatch) => {
  return {
    actions: bindActionCreators({ ...messageActions }, dispatch)
  }
}

export default connect(mapState, mapDispatch)(ChatComponent)