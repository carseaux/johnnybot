class Chatbot {
  constructor () {
    this.current = 0
    this.questions = [
      { index: 0, type: 'question', question: 'Are you gay?', answers: ['Yes', 'No', 'Maybe'] },
      { index: 1, type: 'question', question: 'Are you sure?', answers: ['Yes, ofcourse', 'Are you kidding me?', 'No..'] }
    ]
  }

  requestNextQuestion (prevAnswerData) {
    const next = this.current + 1
    this.current++
    
    if (this.questions[next]) {
      return this.questions[next]
    }
    return null
  }
}

export default new Chatbot()