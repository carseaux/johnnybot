import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import AppWrapper from './components/app-wrapper'
import IndexRoute from './routes/index'
import store from './store'

ReactDOM.render(
	<Provider store={store}>
		<Router>
			<AppWrapper>
				<Switch>
					<Route path="/" component={ IndexRoute } />
				</Switch>
			</AppWrapper>
		</Router>
	</Provider>,
	document.getElementById('root')
)